import time

import scrapy
from scrapy.loader import ItemLoader

from waste.items import WasteItem


class WasteSpider(scrapy.Spider):
    name = "waste"
    start_urls = ["https://reciclario.com.ar/"]

    def parse(self, response):
        for category in response.xpath('//div[@id="content"]/div'):
            category_url = category.xpath('.//a/@href').get()
            next_page = response.urljoin(category_url)
            yield scrapy.Request(next_page, callback=self.parse_category)

    def parse_category(self, response):
        for product in response.xpath('//div[@id="content"]/ul/li'):
            item_url = product.xpath('.//a/@href').get()
            next_page = response.urljoin(item_url)
            yield scrapy.Request(next_page, callback=self.parse_item)

    @staticmethod
    def parse_item(response):
        time.sleep(1)
        item = ItemLoader(WasteItem(), response)
        item.add_xpath('category', '//*[@id="sidebar"]/h3/text()')
        item.add_xpath('name', '/html/body/div[3]/section/div/div/div[2]/div[2]/div[1]/h1/text()')
        item.add_xpath('image', '//*[@id="content"]/div[2]/div[1]/div[1]/img/@src')
        item.add_xpath('where_i_throw_it', '//*[@id="content"]/div[2]/div[2]/div[3]/ul/li/@class')
        item.add_xpath('type_of_material', '//*[@id="content"]/div[2]/div[2]/div[4]/ul/li/text()')
        item.add_xpath('ranking', '//*[@id="content"]/div[2]/div[2]/div[5]/div[2]/span/@class')
        yield item.load_item()
