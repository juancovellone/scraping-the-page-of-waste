def ranking_value(value):
    return int(value[-1])


def format_category(value):
    return str(value).lower()


def format_of_where_i_throw_it(value):
    return str(value).replace(' tt', '').replace('_', ' ')
