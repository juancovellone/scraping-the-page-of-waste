import scrapy
from itemloaders.processors import MapCompose, TakeFirst
from scrapy.item import Field
from w3lib.html import remove_tags

from waste.utils import format_category, format_of_where_i_throw_it, ranking_value


class WasteItem(scrapy.Item):
    name = Field(
        input_processor=MapCompose(remove_tags),
        output_processor=TakeFirst(),
    )
    image = Field(
        input_processor=MapCompose(remove_tags),
        output_processor=TakeFirst(),
    )
    category = Field(
        input_processor=MapCompose(remove_tags, format_category),
        output_processor=TakeFirst(),
    )
    where_i_throw_it = Field(input_processor=MapCompose(remove_tags, format_of_where_i_throw_it))
    type_of_material = Field()
    ranking = Field(
        input_processor=MapCompose(remove_tags, ranking_value),
        output_processor=TakeFirst(),
    )
