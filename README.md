# Scraping the page of waste

Install libraries

```
pip install -r requirement.txt
```

Run scrapy

```
cd waste/waste
scrapy runspider spiders/waste_spider.py -O residuos.json
```
